# -*- coding: utf-8 -*-

import keyboard
import mouse
import subprocess
import os
import PySimpleGUI as sg

WIN_NAME = "\"World Of Warcraft\""
ACCEPTED_KEYS = {
    '&':"1",
    'é':"2",
    '\"':"3",
    '\'':"4",
    '(':"5",
    '-':"6",
    '_':"7",
    'ç':"8",
    'ç':"9",
    'à':"0",
    ')':"U0029",
    "=":"U003D",
    "j":"j"
}
downlist = []

def is_done(id, propagated):
    for done_id in propagated:
        if abs(int(done_id) - int(id)) < 10 :
            return True
    return False

def k_propagate(event):

    #Tolerated keys
    global ACCEPTED_KEYS

    #Already propagated windows
    propagated = []

    #Key down list so we don't press key down 5xx times
    global downlist

    if event.name in ACCEPTED_KEYS :

        print(event)

        #If we already pressed the same downkey before and we try to press it again we just skip the input
        if event.event_type == 'down' and event.name in downlist:
            return

        #Get front window
        front_window = subprocess.check_output("xdotool getactivewindow", shell=True)
        propagated.append(front_window)

        #Get window names
        query = subprocess.check_output("xdotool search --name " + WIN_NAME, shell=True).split()
        for id in query:

            #If window has not been propagated we do it now
            if not is_done(id, propagated):
                subprocess.Popen(['xdotool', 'key' , '--window', id, ACCEPTED_KEYS[event.name]],
                stdout=subprocess.PIPE, 
                stderr=subprocess.STDOUT)

                #TODO: GUI FOR ENABLING DISABLING AUTOMATICLY INTERACT ON 1 
                if ACCEPTED_KEYS[event.name] == "1":
                    subprocess.Popen(['xdotool', 'key' , '--window', id, ACCEPTED_KEYS["j"]],
                    stdout=subprocess.PIPE, 
                    stderr=subprocess.STDOUT)
        
        #Key has been propagate so we update the downlist
        if(event.event_type == 'down'):
            downlist.append(event.name)
        elif(event.event_type == 'up'):
            downlist.remove(event.name)

sg.theme('DarkAmber')	# Add a touch of color

# All the stuff inside your window.
layout = [  [sg.Button('Start'), sg.Button('Stop')] ]

# Create the Window
window = sg.Window('Multiboxing', layout)

# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event is None:	# if user closes window
        break
    if event == 'Start':
        keyboard.hook(k_propagate)
    if event == 'Stop':
        keyboard.unhook_all()

window.close()


